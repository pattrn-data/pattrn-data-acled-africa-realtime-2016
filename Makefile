.PHONY: pattrn-data clean

PATTRN_DATA_FOLDER := pattrn-data/data/pattrn-data-acled-africa-realtime-2016

all: pattrn-data

clean:
	rm -rf ./tmp/*
	rm -rf ./pattrn-data

pattrn-data:
	mkdir -p $(PATTRN_DATA_FOLDER)
	mkdir -p ./tmp
	cp source-metadata/config.json pattrn-data/config.json
	cp source-metadata/metadata.json $(PATTRN_DATA_FOLDER)/metadata.json
	cp source-metadata/settings.json $(PATTRN_DATA_FOLDER)/settings.json
	Rscript scripts/acled.R
	cp tmp/data.geojson $(PATTRN_DATA_FOLDER)/data.geojson
